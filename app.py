from flask import Flask, request, Response, jsonify
from flask_pymongo import PyMongo
from werkzeug.security import generate_password_hash, check_password_hash

import json

app = Flask(__name__)

app.secret_key = "secret"

app.config["MONGO_URI"] = "mongodb://localhost:27017/VnExpress"

mongo = PyMongo(app)


@app.route('/')
def index():
    return str("Well come to my code!")


@app.route('/add', methods=['POST'])
def add_user():
    """To add new user
    :return: result
    """
    _json = request.json or None
    if 'name' and 'email' and 'pwd' in _json:
        _name = _json['name']
        _email = _json['email']
        _password = _json['pwd']
        if _name and _email and _password and request.method == 'POST':
            _hashed_password = generate_password_hash(_password)
            mongo.db.user.insert({'name': _name, 'email': _email, 'pwd': _hashed_password})
            resp = jsonify("User added successfully")
            resp.status_code = 200
            return resp
        else:
            return not_found()
    else:
        return not_found()


@app.errorhandler(404)
def not_found(error=None):
    """To show error
    :param error:
    :return:
    """
    message = {
        'Status': 404,
        'message': 'Not Found ' + request.url
    }
    resp = jsonify(message)
    resp.status_code = 404
    return resp


@app.route('/users')
def users():
    """To show users

    :return: users
    """
    all_users = dict()
    count = 0
    users_cur = mongo.db.user.find()
    for user in users_cur:
        count += 1
        all_users[f"the {count} user"] = user
    return str(all_users)


@app.route('/api/page-<int:page>')
def show_page(page):
    """Show article follow one of page

    :param page: the page number
    :return: the result
    """
    page_result = {}
    article_number = 0
    page_number = 0
    count = mongo.db.VnExpressArticle.count()

    if (count % 20) == 0:
        page_number = count // 20
    else:
        page_number = count // 20 + 1
    if page > page_number or page < 1:
        return not_found()
    else:
        articles = mongo.db.VnExpressArticle.find().limit(20).skip(20 * (page - 1))
        for article in articles:
            article_number += 1
            page_result[f'The article {article_number}'] = {'Title': article['Title'],
                                                            'Content': article['Content'],
                                                            'Comment': article['Comment']
                                                            }
        page_result["detail"] = f"page {page} of {page_number}"
        return jsonify(page_result)


@app.route('/api/search', methods=['GET'])
def search():
    """To search article
    :return: result
    """
    article_result = dict()
    article_number = 0

    post = request.json or {}
    if 'title' and 'content' in post and request.method == 'GET':
        articles = mongo.db.VnExpressArticle.find({'Title': post['title']},
                                                  {'Content': post['content']})
    elif 'title' in post and request.method == 'GET':
        articles = mongo.db.VnExpressArticle.find({'Title': post['title']})
    elif 'content' in post and request.method == 'GET':
        articles = mongo.db.VnExpressArticle.find({'Content': post['content']})
    elif request.method == 'GET':
        return jsonify("Please insert the content or title!")
    else:
        return not_found()

    for article in articles:
        article_number += 1
        article_result[f'The article {article_number}'] = {'Title': article['Title'],
                                                           'Content': article['Content'],
                                                           'Comment': article['Comment']
                                                           }
    article_result["detail"] = f"Have {article_number} result was found!"
    return jsonify(article_result)


@app.route('/api/edit', methods=['POST'])
def edit_article():
    """To edit article follow request

    :return: edit article
    """
    if request.method == 'POST':
        _json = request.json or None
        if 'name' and 'email' and 'pwd' in _json:
            _name = _json['name'] or None
            _email = _json['email'] or None
            _password = _json['pwd']
            if (_name or _email) and _password:
                pwd = mongo.db.user.find_one({'email': _email} or {'name': _name})
                if check_password_hash(pwd['pwd'], _password):
                    if 'add' in _json:
                        if 'title' and 'content' in _json['add']:
                            mongo.db.VnExpressArticle.insert({'Title': _json['add']['title']},
                                                             {'Content': _json['add']['content']})
                            resp = jsonify("Article was added successfully")
                            resp.status_code = 200
                            return resp
                        else:
                            return jsonify('Please insert the content and title')

                    elif 'delete' in _json:
                        if 'title' and 'content' in _json['delete']:
                            if mongo.db.VnExpressArticle.remove({'Title': _json['delete']['title']},
                                                                {'Content': _json['delete']['content']}):
                                resp = jsonify("User added successfully")
                                resp.status_code = 200

                            else:
                                resp = jsonify("Delete not  success!")
                                resp.status_code = 200
                            return resp
                        else:
                            return jsonify('Please insert the content and title')

                    elif 'update' in _json:
                        if 'title_old' and 'title_new' and 'content_new' in _json['update']:
                            if mongo.db.VnExpressArticle.update({'Title': _json['update']['title_old']},
                                                                {'$set': {'Title': _json['update']['title_new'],
                                                                          'Content': _json['update']['content_new']}}):
                                resp = jsonify("User added successfully")
                                resp.status_code = 200
                            else:
                                return jsonify("Update have not succeed!")
                        else:
                            return jsonify('Please insert the content and title')
                    else:
                        return jsonify('Please choose method')

                else:
                    return jsonify("Please check password or user!")
            else:
                return not_found()
        return jsonify("Succeed!")
    else:
        return not_found()


if __name__ == '__main__':
    app.run(debug=True)
